# Deep Learning

!!! warning "Upcoming changes to Cori GPU modulefiles"
    Cori GPU modulefiles are moving to a new location. Workflows on
    this system must be adjusted to account for this new change. Please see
    [here](modulefile-changes.md) for more information.

Cori GPU provides several modules to different machine learning and deep
learning tools and libraries.

## cuDNN

[cuDNN](https://developer.nvidia.com/cudnn) is NVIDIA's library of
GPU-accelerated primitives for deep neural networks. It is available as the
`cudnn` module, and is loaded automatically when a `cuda` module is loaded.

## TensorFlow

[TensorFlow](https://www.tensorflow.org) is an open-source toolkit for machine
learning. It is available as the module `tensorflow/gpu-<version>`. Users are
also encouraged to use GPU-optimized [TensorFlow
containers](https://ngc.nvidia.com/catalog/containers/nvidia:tensorflow)
provided on NVIDIA's [NGC container
library](https://www.nvidia.com/en-us/gpu-cloud/). NGC containers can run on
Cori GPU using [Shifter](https://docs.nersc.gov/development/shifter/overview/).

## PyTorch

[PyTorch](https://pytorch.org) is an open-source machine learning framework. It
is available as the module `pytorch/<version>-gpu`. Users are also encouraged
to use GPU-optimized [PyTorch
containers](https://ngc.nvidia.com/catalog/containers/nvidia:pytorch) from NGC.
