#!/bin/bash

module load spin
export RANCHER_ENVIRONMENT=dev-cattle
rancher up -f spin/docker-compose.yml -s development-systems-docs --force-upgrade -d
rancher up -f spin/docker-compose.yml -s development-systems-docs --confirm-upgrade -d

