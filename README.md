# NERSC Technical Documentation

This repository contains NERSC technical documentation written in Markdown
which is converted to html/css/js with the [mkdocs](http://www.mkdocs.org)
static site generator. The [theme](https://gitlab.com/NERSC/mkdocs-material) is
a fork of [mkdocs-material](https://github.com/squidfunk/mkdocs-material) with
NERSC customizations such as the colors.
